<?php


add_image_size( 'thumbs', 356, 475, true );

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function my_theme_enqueue_styles() {     
    
    $path_js = get_stylesheet_directory_uri() . '/assets/js/';
 	$path_css = get_stylesheet_directory_uri() . '/assets/css/';
            		
	wp_deregister_script( 'wp-embed' );
	 	
	// wp_localize_script( 'jquery', 'siteVars', [] );						
    wp_enqueue_script('jquery-mask', '//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js', ['jquery'], false, true);

	/* Choices JS */
	/* ----------------------------------------- */
		wp_enqueue_script('choices-js', '//cdn.jsdelivr.net/npm/choices.js@4.1.0/public/assets/scripts/choices.min.js', ['jquery'], false, true);
		// wp_enqueue_style( 'choices-css', '//cdn.jsdelivr.net/npm/choices.js@4.1.0/public/assets/styles/choices.min.css');
	/* ----------------------------------------- Choices JS */
		
	/* Caleran JS */
	/* ----------------------------------------- */
		// wp_enqueue_script('moment-js', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js', ['jquery'], false , true);
		// wp_enqueue_script('momentjs-ptbr', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/locale/pt-br.js', ['moment-js'], '2.20.1', true );
		// wp_enqueue_script('caleran-js', $path_js.'caleran.min.js', ['jquery', 'moment-js'], false , true);
		// wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	/* ----------------------------------------- Caleran JS */
	
	/* Scroll Magic */
	/* ----------------------------------------- */
		wp_enqueue_script('scrollmagic', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', [], false, true);				
		// wp_enqueue_script('scrollmagic--debug', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', ['scrollmagic'], false, true);
	/* ----------------------------------------- Scroll Magic */
	
	/* Slick */
	/* ----------------------------------------- */
		wp_enqueue_script('slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', ['jquery'], false, true);
		wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');	
	/* ----------------------------------------- Slick */
	

	/* GSAP */
	/* ----------------------------------------- */
		wp_enqueue_script('gsap', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', ['jquery'], false, true);		
		// wp_enqueue_script('gsap-css-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSPlugin.min.js', [], false, true);				
		// wp_enqueue_script('gsap-css-rule-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSRulePlugin.min.js', [], false, true);				
		wp_enqueue_script('scrollmagic--gsap', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js', [], false, true);		
	/* ----------------------------------------- GSAP */
	
	
	/* Custons */
	/* ----------------------------------------- */
		wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Roboto:400,500|Rye');
		wp_enqueue_style( 'bootstrap', $path_css . 'bootstrap/bootstrap.css');
	  wp_enqueue_style( 'main', $path_css . 'main.css');
		
	/* ----------------------------------------- Custons */	
}


function do_my_shortcode($shortcode) {
    if (!$shortcode) return false;

    ob_start();
    echo do_shortcode($shortcode);
    $output = ob_get_contents();
    ob_end_clean();

    // Adiciona placeholder nos selects
    $output = str_replace('option value=""', 'option placeholder ', $output);
        
    return $output; 

}




function pp_set_bg_mask($data) {
	
	// echo '<pre>'.print_r($data,1). '</pre>';
	// die();
	$desktopSource = isset($data['sizes']['thumbs']) ? $data['sizes']['thumbs'] : $data;	
	$desktop = isset($desktopSource) && $desktopSource ?  $desktopSource : false;
    // $mobile = isset($data['bg_mobile']) && $data['bg_mobile'] ?  $data['bg_mobile'] : false;
    
    // $desktop = $desktop && !is_array($desktop) ? wp_get_attachment($desktop) : $desktop;
    // $mobile = $mobile && !is_array($mobile) ? wp_get_attachment($mobile) : $mobile;
    

    if ($mobile) {
      echo '<div class="lazy lazy--background pp-bg-mobile d-expand-none" data-bg="url(\''.$mobile['url'].'\')"></div>';
    }

    if ($desktop) {
      echo '<div class="lazy lazy--background pp-bg-desktop" data-bg="url(\''.$desktop.'\')"></div>';
    }

    if (isset($data['mask']) && $data['mask'] ) {
      echo pp_set_mask();
    }
  }


  function add_inline_scripts_to_footer() { ?>
	
	<script type="text/javascript">
		<?php // Lazyload https://github.com/verlok/lazyload ?>
		(function (w, d) {
			w.addEventListener('LazyLoad::Initialized', function (e) {
				w.lazyLoadInstance = e.detail.instance;
			}, false);
			var b = d.getElementsByTagName('body')[0];
			var s = d.createElement("script"); s.async = true;
			var v = !("IntersectionObserver" in w) ? "8.16.0" : "10.19.0";
			s.src = "https://cdn.jsdelivr.net/npm/vanilla-lazyload@" + v + "/dist/lazyload.min.js";
			w.lazyLoadOptions = {
				elements_selector: ".lazy",
				callback_enter: function(element) {
					logElementEvent('ENTERED', element);
				},
				callback_set: function(element) {
					logElementEvent('SET', element);
				},
				callback_error: function(element) {
					logElementEvent('ERROR', element);
					// element.src = 'https://placeholdit.imgix.net/~text?txtsize=21&txt=Fallback%20image&w=220&h=280';
				},
			};
			b.appendChild(s);
		}(window, document));
		function logElementEvent(eventName, element) {
			console.log(Date.now(), eventName, element.getAttribute('data-bg'));
		}			
	</script>
<?php
}
add_action( 'wp_footer', 'add_inline_scripts_to_footer' );


/**
 * Register widgetized areas 
 */
function twentyten_widgets_init_child() {	

	register_sidebar( array(
		'name' => __( 'Languages', 'pp' ),
		'id' => 'sidebar-language',
		'description' => __( 'Arraste os itens desejados até aqui. ', 'pp' ),
		'before_widget' => '<div class="widget %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 style="display:none;">',
		'after_title' => '</h2>',
	) );

}

/** Register sidebars by running twentyten_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'twentyten_widgets_init_child' );


add_filter( 'timber_context', 'mytheme_timber_context_language'  );

function mytheme_timber_context_language( $context ) {
	$context['languageBar'] = Timber::get_widgets('sidebar-language');
	return $context;
}

add_filter('ACFFA_always_enqueue_fa', function() { return true; } ); 

function get_last_insta() {
	the_widget('null_instagram_widget', [
			'username' => '@' . get_field('usuario_insta'),
			'number' => 4
	]);
}

add_filter('wpiw_list_class', function() { return 'gallery'; });


function my_cptui_change_posts_per_page( $query ) {
	if ( is_admin() || ! $query->is_main_query() ) {
		 return;
	}

	if ( is_post_type_archive( 'artista' ) || is_post_type_archive( 'bodypiercing' )) {
		 $query->set( 'posts_per_page', -1 );
	}
}
add_filter( 'pre_get_posts', 'my_cptui_change_posts_per_page' );
