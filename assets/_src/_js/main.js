/* eslint-disable one-var */
/* eslint-disable no-undef */
/* eslint-disable no-tabs */
// import smoothscroll from 'smoothscroll-polyfill';
import fancybox from '@fancyapps/fancybox';


/* Variables */
/* ----------------------------------------- */
	const expandWidth = 768;
/* ----------------------------------------- Variables */

/* After Expand */
/* ----------------------------------------- */
	// if ($(window).width() < expandWidth) {}
/* ----------------------------------------- After Expand */

/* Default Scripts */
/* ----------------------------------------- */
	// Mascara de DDD e 9 dígitos para telefones
	let SPMaskBehavior = function(val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		}, spOptions = {onKeyPress: function(val, e, field, options) {
		field.mask(SPMaskBehavior.apply({}, ...e), options);
	}};
	$('.mask-phone, input[type="tel"], .wpcf7-tel').mask(SPMaskBehavior, spOptions);
	// SmoothScroll
	// smoothscroll.polyfill();
	// window.scroll({ top: 0, left: 0, behavior: 'smooth' });
	// document.querySelector('header').scrollIntoView({ behavior: 'smooth' });
/* ----------------------------------------- Default Scripts */

/* Mobile Menu */
/* ----------------------------------------- */
// 	const tlMenu = new TimelineMax({paused: true, onComplete: function() {
// 		console.log('Foi');
// 	}});
// 	tlMenu.set('.navbar--menu li', {y: 40, autoAlpha: 0});
// 	tlMenu.to('.navbar--container', .4, {autoAlpha: 1, ease: Power0.easeNone});
// 	tlMenu.staggerTo('.navbar--menu li', .55, {y: 0, autoAlpha: 1, ease: Power4.easeOut}, .25);

// 	if ($(window).width() <= expandWidth) {
// 		$('.js-toggle-menu').on('click', function() {
// 			if (tlMenu.progress() == 1) {
// 				tlMenu.timeScale(1.5).reverse();
// 			} else {
// 				tlMenu.timeScale(1).play();
// 			}
// 		});
// 	}
// /* ----------------------------------------- Mobile Menu */

/* Mobile Menu */
/* ----------------------------------------- */
	let tlMenu = new TimelineMax({paused: true, yoyo: true});

	// Animate itens on timeLineMax
	tlMenu.set('.navbar *', {clearProps: 'all'});
	tlMenu.set('.navbar--menu > li', {y: 40, autoAlpha: 0});
	tlMenu.set('.navbar--container', {zIndex: 10});
	tlMenu.to('.navbar--container', .4, {autoAlpha: 1, ease: Power0.easeNone});
	tlMenu.staggerTo('.navbar--menu > li', .55, {y: 0, autoAlpha: 1, ease: Power4.easeOut}, .25);

	// Toogle menu
	$('.js-toggle-menu').on('click', function() {
		hideSubmenuOnMobile();
		// If the timeline has finished
		if (tlMenu.progress() == 1) {
			closeMenu(tlMenu);
		} else {
			openMenu(tlMenu);
		}
	});
	function openMenu(tlMenu) {
		tlMenu.timeScale(1).play();
	}
	function closeMenu(tlMenu) {
		tlMenu.timeScale(1.5).reverse();
	}
	function hideSubmenuOnMobile() {
		if ($(window).width() < 768) {
			$.each($('.navbar--menu .sub-menu'), function(i, v) {
				$(v).slideUp();
			});
			$('.menu-item-has-children > a').removeClass('active');
		}
	}
	$(window).on('resize', function() {
		hideSubmenuOnMobile();
		// Close the menu if resize to tablet/desktop
		if ($(window).width() >= 768 && tlMenu.progress() === 1) {
			tlMenu.reverse().progress(0);
		}
	});

	$('.navbar--menu .menu-item-has-children > a').on('click', function(e) {
		e.preventDefault();
		if ($(window).width() < 768) {
			$(this)
				.toggleClass('active')
				.next().slideToggle();
		} else {
			$('.navbar--menu .sub-menu.active').not($(this).next()).removeClass('active');
			// console.log();
			$(this).next().toggleClass('active');
		}
	});
/* ----------------------------------------- Mobile Menu */

/* Tabs */
/* ----------------------------------------- */
	let $tabs = $('.tab--header--item');

	$tabs.click(function() {
		if ($(window).width() <= expandWidth) return false;
		let parent = $(this).closest('.tab');
		let bodys = parent.find('.tab--body--item');
		let selected = $(this).index();

		TweenMax.to(parent.find('.tab--body--item'), 0.35, {
			autoAlpha: 0,
			className: '-=active',
			// height: 0,
			onComplete: function() {
				TweenMax.to(bodys[selected], .5, {
					autoAlpha: 1,
					height: 'auto',
					className: '+=active',
				});
			},
		});
		parent.find('.tab--header--item').removeClass('active');
		$(this).addClass('active');
	});
/* ----------------------------------------- Tabs */

/* Formulários */
/* ----------------------------------------- */
let selects = $('select');
if (selects.length) {
	$('select').each((i, el) => {
		let placeholder = $(el).find('option[value=""]');
		placeholder.attr('placeholder', '').change();
		new Choices(el, {
			placeholder: true,
			placeholderValue: placeholder.text(),
			itemSelectText: 'Clique para selecionar',
		});
	});
	}
/* ----------------------------------------- Formulários */

/* Accordion */
/* ----------------------------------------- */
	$('.acc--item').click(function() {
		let $this = $(this),
			$content = $this.find('.acc--item--body');
		if (!$this.hasClass('closed')) {
			TweenLite.to($content, 0.2, {height: 0});
			$this.addClass('closed');
		} else {
			TweenLite.set($content, {height: 'auto'});
			TweenLite.from($content, 0.2, {height: 0});
			$this.removeClass('closed');
		}
	});
/* ----------------------------------------- Accordion */

/* Slick */
/* ----------------------------------------- */
	$('.slick--beforeexpand').slick({
		responsive: [
		{
			breakpoint: 9999,
			settings: 'unslick',
		},
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 3,
				dots: true,
			},
		},
		{
			breakpoint: 576,
			settings: {
				dots: true,
				infinite: false,
			},
		},
		],
	});
/* ----------------------------------------- Slick */
/* Formulário UPload */
/* ----------------------------------------- */
	/* Brought click function of fileupload button when text field is clicked*/
	$('#uploadtextfield').click(function() {
		$('#fileuploadfield').click();
	});

	/* Brought click function of fileupload button when browse button is clicked*/
	$('#uploadbrowsebutton').click(function() {
		$('#fileuploadfield').click();
	});

	/* To bring the selected file value in text field*/
	$('#fileuploadfield').change(function() {
		$('#uploadtextfield').val($(this).val());
	});
/* ----------------------------------------- Formulário UPload */

/* ScrollMagic */
/* ----------------------------------------- */
	const controller = new ScrollMagic.Controller();

	// Fixa o menu na home após scroll
    let fixaMenuHome = new ScrollMagic.Scene({
		triggerElement: 'body.home.desktop header.header',
		triggerHook: 'onLeave',
	})
	.setPin('body.home.desktop header.header')
	.addTo(controller)
	// .addIndicators()
	.on('start', function(event) {
		$('.menu-item-home').toggleClass('ontop');
	});

	// Aumenta o bg conforme rolagem
	let scaleFeatured = new ScrollMagic.Scene({
		triggerElement: '#content',
		triggerHook: 'onEnter',
		duration: '100%',
	})
	.setTween(TweenMax.to('#featured .pp-bg-desktop', 1, {scale: 1.1}))
	.addTo(controller);

	// esconde o bg conforme rolages
	let hideFeatured = new ScrollMagic.Scene({
		triggerElement: '#content',
		triggerHook: 0.5,
		duration: '70%',
	})
	.setTween(TweenMax.to('#featured .pp-bg-desktop', 1, {autoAlpha: 0}))
	.addTo(controller);
/* ----------------------------------------- ScrollMagic */
